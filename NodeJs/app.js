// App.js fait appel aux différentes fonctions implémentées dans l'APi : Accès aux images, aux route User, aux route Sauce

// import des modules npm - Ajout des plugins externes
const express = require('express');

//  accès au chemin de notre système de fichier
const path = require('path');
// onsécurise notre requete http avec helmet
const helmet = require('helmet');
// const sanitizerPlugin = require('mongo-sanitize')
const app = express();
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// importation mongoose pour pouvoir utiliser la base de données
const mongoose = require('mongoose');

// importation des routes pour les sauces
const sauceRoute = require('./routes/produits')

// importation des routes pour les users
const userRoute = require ('./routes/user')

// Connection à la base de données MongoDB avec la sécurité vers le fichier .env pour cacher le mot de passe
mongoose.connect('mongodb+srv://sabrinamichee:MPcluster0.nowhf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
    
useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));

  // Middleware Header pour contourner les erreurs en débloquant certains systèmes de sécurité CORS, afin que tout le monde puisse faire des requetes depuis son navigateur
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  res.setHeader('Content-Security-Policy', "default-src 'self'");
  next();
});
// enregistrement des routes
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use(helmet());
// app.use(sanitizerPlugin());
app.use('/api/produits', produitRoute);
app.use('/api/auth', userRoute);

module.exports = app;
