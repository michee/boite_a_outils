// Récupération du modèle 'produitref'
const Produit = require('../models/Produit');

// Récupération du module 'file system' de Node permettant de gérer ici les téléchargements et modifications d'images
// fs = fileSystème est l’outil adapté pour manipuler des fichiers sur la machine. Grâce à lui vous pourrez créer, lire, 
// déplacer, supprimer des fichiers. Dans le cas d’un serveur web, c’est ce module qui est utilisé pour créer des fichiers 
// de logs. Dans le cas d’un outil CLI, vous pourrez développer des scripts de rangement de fichiers ou d’archivage.
const fs = require('fs');
const produit = require('../models/produit');

// création un nouveau produit
exports.createProduit = (req, res, next) => {

  const produitObject = JSON.parse(req.body.produit)
  delete produitObject._id;
  console.log(produitObject);
  const produit = new produit({
    ...produitObject,
    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`,
    likes: 0,
    dislikes: 0,
    usersLiked: [],
    usersDisliked: []
  });
  // Sauvegarde de la produit dans la base de données
  produit.save()
    .then(() => res.status(201).json({
      message: 'produit enregistrée !'
    }))
    .catch(error => res.status(400).json({
      error
    }));

};

// Sauvegarde de la produit dans la base de données mongoDB
exports.modifProduit = (req, res, next) => {
  const produitObject = req.file ? {
    ...JSON.parse(req.body.Produit),
    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
  } : {
    ...req.body
  };
  Produit.updateOne({
      _id: req.params.id
    }, {
      ...req.body,
      _id: req.params.id
    })
    .then(() => res.status(200).json({
      message: 'Produit modifiée !'
    }))
    .catch(error => res.status(400).json({
      error
    }));

};

// suppression d'une Produit
exports.deleteProduit = (req, res, next) => {
  Produit.findOne({
      _id: req.params.id
    })
    .then(Produit => {
      // récupération de l'image par le nom pour suppression
      const filename = Produit.imageUrl.split('/images/')[1];
      fs.unlink(`images/${filename}`, () => {
        Produit.deleteOne({
            _id: req.params.id
          })
          .then(() => res.status(200).json({
            message: 'image supprimé !'
          }))
          .catch(error => res.status(400).json({
            error
          }));

      });
    })
    .catch(error => res.status(500).json({
      error
    }));

};

// récuperation d'une Produit
exports.getOneProduit = (req, res, next) => {
  Produit.findOne({
      _id: req.params.id
    })
    .then(produit => res.status(200).json(produit))
    .catch(error => res.status(404).json({
      error
    }))

};

// récupération de toutes les produits
exports.getAllProduit = (req, res, next) => {
  Produit.find()
    .then(produit => res.status(200).json(produit))
    .catch(error => res.status(400).json({
      error
    }));
};
exports.likeDislike = (req, res, next) => {
  // Pour la route READ = Ajout/suppression d'un like / dislike à une produit
  // Like présent dans le body
  let like = req.body.like
  // On prend le userID
  let userId = req.body.userId
  // On prend l'id de la produit
  let produitId = req.params.id
 
    
  console.log(req.body);
    if (like === 1) { // Si il s'agit d'un like
    Produit.updateOne({
          _id: produitId
        }, {
          // On push l'utilisateur et on incrémente le compteur de 1
          $push: {
            usersLiked: userId
          },
          $inc: {
            likes: +1
          }, // On incrémente de 1
        })
        .then(() => res.status(200).json({
          message: 'j\'aime ajouté !'
        }))
        .catch((error) => res.status(400).json({
          error
        }))
    }
    if (like === -1) {
      Produit.updateOne( // S'il s'agit d'un dislike
          {
            _id: produitId
          }, {
            $push: {
              usersDisliked: userId
            },
            $inc: {
              dislikes: +1
            }, // On incrémente de 1
          }
        )
        .then(() => {
          res.status(200).json({
            message: 'Dislike ajouté !'
          })
        })
        .catch((error) => res.status(400).json({
          error
        }))
    }
    if (like === 0) { // Si il s'agit d'annuler un like ou un dislike
      Produit.findOne({
          _id: produitId
        })
        .then((produit) => {
          if (produit.usersLiked.includes(userId)) { // Si il s'agit d'annuler un like
            Produit.updateOne({
                _id: produitId
              }, {
                $pull: {
                  usersLiked: userId
                },
                $inc: {
                  likes: -1
                }, // On incrémente de -1
              })
              .then(() => res.status(200).json({
                message: 'Like retiré !'
              }))
              .catch((error) => res.status(400).json({
                error
              }))
          }
          if (produit.usersDisliked.includes(userId)) { // Si il s'agit d'annuler un dislike
          Produit.updateOne({
                _id: produitId
              }, {
                $pull: {
                  usersDisliked: userId
                },
                $inc: {
                  dislikes: -1
                }, // On incrémente de -1
              })
              .then(() => res.status(200).json({
                message: 'Dislike retiré !'
              }))
              .catch((error) => res.status(400).json({
                error
              }))
          }
        })
        .catch((error) => res.status(404).json({
          error
        }))
    }
}
