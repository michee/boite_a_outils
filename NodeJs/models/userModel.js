// Création d'un model user avec mongoose et importation
const mongoose = require('mongoose');

// Rajout de validateur et plugin de purification
const uniqueValidator = require('mongoose-unique-validator');
// const sanitizerPlugin = require('mongoose-sanitizer-plugin');
// Création schéma mongoose pour créer notre propre modèle utilisateur
const userSchema = mongoose.Schema({
    email: {type: String, require: true, unique: true},
    password: {type: String, require: true},
});

// plugging pour garantir un email unique
userSchema.plugin(uniqueValidator);
// Plugin pour Mongoose qui purifie les champs du model avant de les enregistrer dans la base MongoDB.
// userSchema.plugin(sanitizerPlugin);

// exportation de ce schéma sous forme de modèle 
module.exports = mongoose.model('User', userSchema);

//pour pré-valider les informations avant de les enregistrer : npm install --save mongoose-unique-validator
