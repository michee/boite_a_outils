const mongoose = require('mongoose');
// const sanitizerPlugin = require('mongoose-sanitizer-plugin');
// Création schéma pour un produit
const produitSchema = mongoose.Schema({
    userId: {
        type: String,
        require: true,
    },
    name: {
        type: String,
        require: true,    
    },
    manufacturer: {
        type: String,
        require: true,        
    },
    description: {
        type: String,
        require: true,        
    },
    mainPepper: {
        type: String,
        require: true,      
    },
    imageUrl: {
        type: String,
        require: true
    },
    heat: {
        type: Number,
        require: true
    },
    likes: {
        type: Number,
        require:false
    },
    dislikes: {
        type: Number,
        require:false
    },
    usersLiked: {
        type: [String],
        require:false
    },
    usersDisliked: {
        type: [String],
        require:false
    },
});
// produitSchema.plugin(sanitizerPlugin);
module.exports = mongoose.model('Produit', produitSchema);
