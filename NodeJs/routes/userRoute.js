// On a besoin d'Express donc on l'importe
const express = require('express');

// Création d'un router avec la méthode mise à disposition par Express
const router = express.Router();

// On associe les fonctions aux différentes routes, on importe le controller
const userControl = require ('../controllers/user');

// Création des routes Inscription et Connexion de l'API avec les middlewares
// et les controllers d'authentification et de sécurité qui leur sont appliquées
router.post ('/signup',userControl.signup);
router.post ('/login', userControl.login);

module.exports = router;

