const express = require('express');
const router = express.Router();
const produitControl = require('../controllers/produit')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer.config')

// lien vers router pour créer une sauce
router.post('/', auth, multer, produitControl.createProduit );

// lien vers router pour modifier une sauce
router.put('/:id', auth, multer, ProduitControl.modifProduit);

// lien vers router pour supprimer une sauce
router.delete('/:id', auth, produitControl.deleteProduit);

// lien vers router pour selectionner une sauce
router.get('/:id', auth, produitControl.getOneProduit);

// lien vers router pour selectionner toutes les sauces
router.get('/', auth,  produitControl.getAllProduit);

// liens vers le router pour gerer les likes
router.post('/:id/like', auth, produitControl.likeDislike)

module.exports = router;
