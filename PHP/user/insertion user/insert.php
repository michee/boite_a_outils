$nom = "DUPONT";
$prenom = "Jean";
$email = "jean@dupont.com";
$role = "client";

$pdo = pdo();
$bob = $pdo->prepare("
    INSERT INTO utilisateurs
    (nom, prenom, email, role)
    VALUES (:nom, :prenom, :email, :role)");

$bob->bindParam(':nom', $nom);
$bob->bindParam(':prenom', $prenom);
$bob->bindParam(':email', $email);
$bob->bindParam(':role', $role);
$bob->execute();