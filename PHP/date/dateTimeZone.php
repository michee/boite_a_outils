<?php
$paris = new DateTimeZone(
    'europe/paris'
);
$losAngeles = new DateTimeZone('America/Los_Angeles');
var_dump($paris->getName(), $paris->getLocation());
// obtenir la date du jour et l'heure à l'intant t
$maintenant = new DateTime('now');
// Pour afficher on peut faire aussi avec echo
// echo $maintenant->format ('d/m/Y');
var_dump($maintenant->format ('d/m/Y'));

$rachid = new DateTime('2000-03-24 17:00:00' , $paris);
// echo $rachid;


$vendrediProchain = new DateTime(('next friday'));
var_dump($vendrediProchain);

//instancier un objet DateTime depuis un timestamps unix
$timestamp = new DateTime(('@658914830'));


// recuperer une date dans un format tordu (g= heure, i = minute, s=seconde, u=microseconde)
$formatSpecifique = DateTime::createFromFormat('G/i/s:j/m/Y-u', '7/32/12:3/1/2016-5687');
var_dump($formatSpecifique);


// changer la date
$formatSpecifique->setDate(2022, 2, 28);
// changer l'heure
$formatSpecifique->setTime(13, 48, 28);
// changer le fuseau
$formatSpecifique->setTimezone(new DateTimeZone(
    'europe/paris'
));

// modifier une date
$now = new DateTime('2022-07-27');
$now->modify('+1 day 15:12');