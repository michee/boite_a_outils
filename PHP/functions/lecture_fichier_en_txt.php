<?php

// file_get_contents revoie un contenu d'un fichier sous forme de chaine de caractère
$contenu = file_get_contents('fichier.txt');
echo $contenu;

// readfile revoie un contenu d'un fichier sous forme de chaine de caractère directement sans faire un echo.
readfile('fichier.txt');
