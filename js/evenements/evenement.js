// On crée des variable pour aller chercher les éléménts dans le DOM
let buttonForm = document.querySelector('button');
let nameForm = document.querySelector('#name');
let firstNameForm = document.querySelector('#firstName');
let title = document.querySelector('h1')
let crea = document.querySelector('#btn2')


buttonForm.addEventListener('click', function(){
    //  innerText pour sécuriser l'injection de text
  title.innerText = ('Bonjour '+ firstNameForm.value +' '+ nameForm.value); 
console.log(title);
})

buttonForm.addEventListener("mouseover", function( event ) {  
    
    event.target.style.color = "#fff";
    event.target.style.backgroundColor= "#000";})

buttonForm.addEventListener("mouseleave", function( event ) { 
        event.target.style.color = "";
        event.target.style.backgroundColor= "" ;
      })
//    ici j'ai mis des guillemet vide mais on peut mettre
// removeProperty (pour revenir à l'état initial)    

nameForm.addEventListener("change", function(){
    title.innerText = ('Bonjour' + firstNameForm.value+ ' '+ nameForm.value)
})
firstNameForm.addEventListener("change", function(){
    title.innerText = ('Bonjour' + firstNameForm.value+ ' '+ nameForm.value)
})





// integrer des images
crea.addEventListener('click', function(event){
    if(document.querySelector('#myPictures') != undefined){    
        document.body.removeChild(document.querySelector('#myPictures'))
    }else{
// création d'une div qui va contennir deux div image
let divImage= document.createElement("div");
// 
// rattachement à un parent body
document.body.appendChild(divImage)


// création de deux div
let div1 = document.createElement("div");
let div2= document.createElement('div');
divImage.appendChild(div1);
divImage.appendChild(div2);

// mise en page
divImage.id = 'myPictures';
divImage.style.backgroundColor="#f00"
divImage.style.display='flex';
divImage.style.width= '100%';
divImage.style.height='60vh';
divImage.style.justifyContent='space-evently';
div1.style.width='60%';
div1.style.height='100%';
div1.style.backgroundImage ='url(assets/img/1.jpg)';
div1.style.backgroundPosition='center';
div1.style.backgroundRepeat='no repeat';
div1.style.backgroundSize='cover';
div2.style.width='60%';
div2.style.height='100%';
div2.style.backgroundImage ='url(assets/img/2.jpg)';
div2.style.backgroundPosition='center';
div2.style.backgroundRepeat='no repeat';
div2.style.backgroundSize='cover';}
})

// switcher deux Element
// on déclare des constantes pour aller chercher les éléménts dans le DOM
const btn3= document.querySelector('#btn3');
const divCarre = document.querySelector('.carre');


btn3.addEventListener('click', function(){
    divCarre.classList.toggle('vert');
});




// changer le couleur et le fond d'un texte
  
document.querySelector('#txtColor').addEventListener('change', changeCoul);
document.querySelector("#backColor").addEventListener('change', changeFond)
const text = document.querySelector('#text');
document.querySelector('#reset').addEventListener('click',resetP);


function changeCoul(){
    let couleur=document.querySelector('#txtColor');
    let txtP=document.querySelector('#texte');
    txtP.style.color = couleur.value;
}
function changeFond(){
    let couleur=document.querySelector('#backColor');
    let txtP=document.querySelector('#texte');
    txtP.style.backgroundColor = couleur.value;
}
function resetP() {
    let txtP = document.querySelector('#texte');
    // txtP.style.backgroundColor = '#fff';
    // txtP.style.color = '#000';

    txtP.style.removeProperty('background-color');
    txtP.style.removeProperty('color');
}