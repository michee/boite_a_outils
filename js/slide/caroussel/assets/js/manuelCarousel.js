// La liste des index des carousels
let manualSlideIndexes = [];

//Boutons Prev et Next du document
const nexts = document.querySelectorAll('.next');
const prevs = document.querySelectorAll('.prev');

// Liste de tous les caroussels du document
const listCarousels = document.querySelectorAll('.affFlex');

// Force le tableau des images actuves a 1
for (let elt of listCarousels) {
    manualSlideIndexes.push(1);
}

// Ajout de l'écouteur 'click' sur les nexts
for (let i = 0; i < nexts.length; i++) {
    // console.log(nexts[i]);
    nexts[i].addEventListener('click', () => {
        // Appel de laffichage de l'image.
        // i est le N° du caroussel
        // manualSlideIndexes est l'image active
        switchSlides(i, manualSlideIndexes[i] += 1);
        // console.log(i, manualSlideIndexes[i]);
    });
}
// Ajout de l'écouteur d'évènement sur les prevs
for (let i = 0; i < prevs.length; i++) {
    // console.log(prevs[i]);
    prevs[i].addEventListener('click', () => {
        // Appel de laffichage de l'image.
        // i est le N° du caroussel
        // manualSlideIndexes est l'image active
        switchSlides(i, manualSlideIndexes[i] -= 1);
        // console.log(i, manualSlideIndexes[i]);
    });
}

// Démarrage des carousels existants à leur première image
switchSlidesAll();

function plusSlides(id, nbr) {
    switchSlides(id, manualSlideIndexes[id] += nbr);
}

// Fonction pour afficher une image d'un carousel précis

function switchSlides(id, nbr) {
    const slideContainer = document.querySelectorAll(".affFlex")[id];
    const slides = slideContainer.querySelectorAll(".mySlides");

    if (nbr > slides.length) {
        manualSlideIndexes[id] = 1;
    }
    if (nbr < 1) {
        manualSlideIndexes[id] = slides.length;
    }

    for (let cnt = 0; cnt < slides.length; ++cnt) {
        slides[cnt].style.display = "none";
    }
    slides[manualSlideIndexes[id] - 1].style.display = "block";
}

// Fonction pour démarrer les carousels existants à leur première image
function switchSlidesAll() {
    for (let cnt = 0; cnt < document.querySelectorAll(".affFlex").length; ++cnt) {
        switchSlides(cnt, manualSlideIndexes[cnt]);
    }
}