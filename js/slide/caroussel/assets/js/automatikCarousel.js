let slideIndex = 0, slideIndex2=0;
showSlides();
showSlides2();

function showSlides() {
    let slides = document.querySelectorAll(".mySlides");

    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;

    if (slideIndex > slides.length) {
        slideIndex = 1
    }
    slides[slideIndex - 1].style.display = "block";
    setTimeout(showSlides, 2000); // Change image toutes les 2 secondes
}

function showSlides2() {
    let slides = document.querySelectorAll(".mySlides2");

    for (let i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex2++;

    if (slideIndex2 > slides.length) {
        slideIndex2 = 1
    }
    slides[slideIndex2 - 1].style.display = "block";
    setTimeout(showSlides2, 3000); // Change image toutes les 2 secondes
}