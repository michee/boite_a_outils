console.log('hello ma beauté');

let x = -1;

if (x >= 0) {

} else {

}

x = 2;
if (x == 0) {
    console.log('x est nul');
} else if (x > 0) {

} else {

}

//Ecrire une fonction qui prend en param 3 nombre. Cette fontion fera la somme des 3 nombres seulement
// Si le premier nombre est positif et affichera dans la console le résultat de l'addition.

function cal(a, b, c) {
    if (a >= 0) {

    } else {

    }
}
cal(-1, 2, 3);

function pairOrImpair(nbr) {
    if (nbr % 2 == 0) {

    } else {

    }
}
pairOrImpair(2)


// switch

const element = 'farine';

switch (element.toLocaleLowerCase()) {
    case 'gateau':

        break;
    case 'pain':
    case 'farine':

        break;

    default:

        break;
}

// Opérations ternaires
let test = -5;

//Ecrire une fonction qui prend 1 nbr en param et qui retourne 'pair' s'il est pair et sinon impair en utilisant opérateur 
// ternaire.

impair = (nbr) => {
    return (nbr % 2 == 0 ? 'pair' : 'impair');
}


// Ecrire une fonction qui prend un paramètre et retourne si le paramètre contient un string ou un number (ternaire)

testSting = (x) => {
    return (typeof (x) === 'string' ? "c'est une chaine de caractère" : "c'est un number");
}




// Les boucles

// while

let y = 0;
while (y < 6) {
        y++;
}

// Faire un tableau myTab contenant 10 éléments. Avec la boucle while, afficher la console
// chaque élément du tableau.

let myTab = ["bananes", 'fraises', 'pommes', 'oranges', 'poires', 'mangues', 'kiwis', 'raisin', 'litchis', 'cerises'];
let f = 0;

while (f < myTab.length) {
    f++;
}



// FOR

// for(init, condition, increment){ code};


for (let i = 0; i < 5; i++) {

}
for (let i = 5; i > 0; i--) {

}

// afficher le contenu de myTab dans la console avec boucle for dans un sens et dans l'autre

for (let i = 0; i < myTab.length; i++) {

}

for (let i = myTab.length - 1; i >= 0; i--) {

}
// Ecrire une fonction qui retourne la somme des éléments de la liste. Cette fonction prendra en parametre le nom de la liste.

let liste1 = [10, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let liste2 = [1, 5, 6, 4, 9, 8, 7, 2, 5, 44];

additionNbr = (li) => {
    let sum = 0;
    for (let i = 0; i < li.length; i++) {
        sum += li[i];
    }
    return (sum);
}




// autre méthode

somWhile = (li) => {
    let som = 0;
    let compteur = 0;
    while (compteur < li.length) {
        som += li[compteur];
        compteur++
    }
    return som;
}

// ecrire une fonction qui calcule et retourne la moyenne du tableau.

moyTab = (li) => {
    let sum = 0;
    let moy = 0;
    for (let i = 0; i < li.length; i++) {
        sum += li[i];
        moy = sum / li.length;
    }
    return moy;
}


// autre méthode en reprenant une fonction existante

moyTab2 = (li) => {
    return (additionNbr(li) / li.length)
}


// pour chercher un element dans un tableau
// For....of

// For (let elemt of liste){code}

// exemple :

for (let elt of liste1) {
   
}

// ecrire une fonction qui prend en parametre un nom de liste et un retourne le plus petit élément du tableau.
minChiffre = (li) => {
    let petit = li[0];
    for (let elt of li) {        
        if (elt < petit) {
            petit = elt;
        }        
    }
    return petit;
}

// autre methode
let m = Math.min(...liste1);
console.log(m);

// trouver le plus grand

maxChiffre = (li) => {
    let grand = li[0];
    for (let elt of li) {        
        if (elt > grand) {
            grand = elt;
        }        
    }
    return grand;
}

// autre methode
let max = Math.max(...liste2)
console.log(max);

// nombre aléatoire
// utilisation de Math.random();
// retourne un nombre (float);
// compris entre 0 et 1 (1exclu)

console.log(Math.random());

for (i=0; i < 10 ; i++){
    console.log(Math.random());
}

// ecrire une fonction pile ou face ;
function pileFace (){
    // Attention mettre des guillemets sinon ca ne fonctionne pas.
return (Math.random()<=0.5 ? 'pile' : 'face');   
}
for (i=0; i < 2 ; i++){
console.log(pileFace());}