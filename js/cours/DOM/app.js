// Dom et js

// Accès aux éléments HTML avec la variable document.

let titre = document.querySelector('#titre');
console.log(titre);
titre.style.color = '#f00';
titre.style.backgroundColor = "#000";

const div1 = document.getElementById('myId');
const div2 = document.querySelector('#myId2');

div1.style.width = "150px";
div1.style.height = "150px";
div1.style.border = "solid 1px #f00";
div1.style.borderRadius = "50%"


div2.style.width = "150px";
div2.style.height = "150px";
div2.style.border = "solid 1px #00f";
div2.style.borderRadius = "50%";

// appeler par une class

const myClass = document.getElementsByClassName('myClass')
// Ca retourne un tableau

for (let elt of myClass) {
    elt.style.backgroundColor = "#0f9";
}

// Récupération d'élément par la balise

const maBalise = document.getElementsByTagName('h1');
// aller chercher l'élément à son index []
maBalise[0].style.textAlign = 'center';
maBalise[0].innerText = 'coucou !!!!'

// Version ES6

const toto = document.querySelector('#myId');

const titi = document.querySelector('.myClass');
const tata = document.querySelectorAll('.mylass')
// récupère toutes les class (.myClass)
const tutu = document.querySelectorAll('*');
// je récupère tous les éléments de mon document


// Injection de code

// 1 chercher la div
const inj = document.querySelector('#inject');
// 2 définir une variable texte
let code = "<input type ='text' value='nom' id='nom'>";
code += "<input type ='text' value='prénom' id='prenom'>";
inj.innerHTML = code;

// pour faire une alerte

// const accept = window.alert('payement validé');

// Création de contenu  avec createElement
let txt = document.createElement('p');
txt.innerText = 'Texte ajouté avec createElement';
// Ajout de l'élément dans la page dans son parent
inj.appendChild(txt);

// changer un element

let inpValue = document.querySelectorAll('input');
inpValue[0].value = 'Bar';
inpValue[1].value = 'Lenny';

//  bouger un élement dans le dom

inj.style.display = ('flex');
inj.style.flexDirection = ('column');

// integrer un Element
let texte = document.createTextNode('texte TexteNode');
txt.appendChild(texte);


// On peut créer une fonction pour evité d'écrire queryselector

// let selectTous = function(quoi){
//     return (document.querySelectorAll(quoi))
// };



